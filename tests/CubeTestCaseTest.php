<?php

class CubeTestCaseTest extends TestCase {
    
    /**
     * Test a set of valid test cases
     *
     * @return void
     */
    public function testValidTestCases()
    {    
        $inputs[] = '4 6
            UPDATE 2 2 2 4
            QUERY 1 1 1 3 3 3
            UPDATE 1 1 1 1
            QUERY 2 2 2 4 4 4
            QUERY 1 1 1 3 3 3
            QUERY 1 1 1 3 3 3';        
        $inputs[] = '2 4 
            UPDATE 2 2 2 1
            QUERY 1 1 1 1 1 1
            QUERY 1 1 1 2 2 2
            QUERY 2 2 2 2 2 2';       
        $inputs[] = '2 4 
            UPDATE 2 2 2 1000000000
            QUERY 1 1 1 1 1 1
            QUERY 1 1 1 2 2 2
            QUERY 2 2 2 2 2 2';
                
        foreach($inputs as $input) {
            $input = explode("\n", $input);
            $test_case = new \App\Classes\CubeTestCase();
            $this->assertTrue($test_case->parseInput($input));
        }
    }
    
    /**
     * Test a set of invalid test cases
     *
     * @return void
     */
    public function testInvalidTestCases()
    {    
        $inputs[] = '4 6
            UPDATE 2 2 2 4
            QUERY 1 1 1 3 3 3
            UPDATE 1 1 1 1
            QUERY 2 2 2 4 4 4
            QUERY 1 1 1 3 3 3';      
        $inputs[] = '4 6
            UPDATE 2 2 2 4
            QUERY 1 1 1 3 3 3
            UPDATES 1 1 1 1
            QUERY 2 2 2 4 4 4
            QUERY 1 1 1 3 3 3
            QUERY 1 1 1 3 3 3';       
        $inputs[] = '2 4 
            QUERY 1 1 1 1 1 1
            QUERY 1 1 1 2 2 2
            QUERY 2 2 2 2 2 2';    
        $inputs[] = '2 4 
            UPDATE 2 2 2 1
            QUERY 1 1 1 1 1 1
            QUERY 1 1 1 2 2 2
            QUERY 2 2 2 2 2 3';       
        $inputs[] = '2 4 
            UPDATE 2 2 2 1
            QUERY 1 1 1 1 1 1
            QUERY 1 1 1 2 2 2
            QUERY 2 2 2 2 2 -2';      
        $inputs[] = '2 4 
            UPDATE 2 2 3 1
            QUERY 1 1 1 1 1 1
            QUERY 1 1 1 2 2 2
            QUERY 2 2 2 2 2 2';
        
        
        foreach($inputs as $input) {
            $input = explode("\n", $input);
            $test_case = new \App\Classes\CubeTestCase();
            $this->assertFalse($test_case->parseInput($input));
        }
    }
    
}
