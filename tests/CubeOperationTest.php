<?php

class CubeOperationTest extends TestCase {
    
    /**
     * Test a set of valid operations of type QUERY
     *
     * @return void
     */
    public function testValidQueryOperations()
    {
        $N = 3;        
        $inputs[] = 'QUERY 1 1 1 3 3 3';
        $inputs[] = 'QUERY 1 2 3 1 2 3';
        $inputs[] = 'QUERY 2 2 2 2 2 3';
        
        $len = count($inputs);
        
        for($i = 0; $i < $len; $i++) {
            $operation = new App\Classes\CubeOperation($N);
            $this->assertTrue($operation->parseInput($inputs));
            
            $this->assertEquals($operation->type, 'QUERY');
            $this->assertCount(6, $operation->parameters);
        }
    }
    
    /**
     * Test a set of valid operations of type UPDATE
     *
     * @return void
     */
    public function testValidUpdateOperations()
    {
        $N = 3;        
        $inputs[] = 'UPDATE 3 3 3 1000000000';
        $inputs[] = 'UPDATE 2 3 1 -1000000000';
        $inputs[] = 'UPDATE 1 1 1 1000000000';
        
        $len = count($inputs);
        
        for($i = 0; $i < $len; $i++) {
            $operation = new App\Classes\CubeOperation($N);
            $this->assertTrue($operation->parseInput($inputs));
            
            $this->assertEquals($operation->type, 'UPDATE');
            $this->assertCount(4, $operation->parameters);
        }
    }
    
    /**
     * Test a set of invalid operations of type QUERY
     *
     * @return void
     */
    public function testInvalidQueryOperations()
    {
        $N = 3;        
        $inputs[] = 'QUERY 1 1 1 3 3 4';
        $inputs[] = 'QUERY 1 3 3 1 2 3';
        $inputs[] = 'QUERY 2 2 2 2 2';
        $inputs[] = 'UDPATE 2 2 2 2 2 2';
        
        $len = count($inputs);
        
        for($i = 0; $i < $len; $i++) {
            $operation = new App\Classes\CubeOperation($N);
            $this->assertFalse($operation->parseInput($inputs));
        }
    }
    
    /**
     * Test a set of invalid operations of type UPDATE
     *
     * @return void
     */
    public function testInvalidUpdateOperations()
    {
        $N = 3;        
        $inputs[] = 'UPDATE 3 3 3 1000000001';
        $inputs[] = 'UPDATE 2 3 1 -1000000001';
        $inputs[] = 'UPDATE 1 1 4 1000000000';
        $inputs[] = 'QUERY 1 1 3 1000000000';
        $inputs[] = 'UPDATE 1 1 4';
        $inputs[] = 'UPDATE 3 3 0 1000000000';
        
        $len = count($inputs);
        
        for($i = 0; $i < $len; $i++) {
            $operation = new App\Classes\CubeOperation($N);
            $this->assertFalse($operation->parseInput($inputs));
        }
    }
}
