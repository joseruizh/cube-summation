<?php

class CubeSummationTest extends TestCase {
    
    /**
     * Test a set of valid Cube inputs
     *
     * @return void
     */
    public function testValidInput()
    {    
        $inputs[] = '2
        4 6
        UPDATE 2 2 2 4
        QUERY 1 1 1 3 3 3
        UPDATE 1 1 1 1
        QUERY 2 2 2 4 4 4
        QUERY 1 1 1 3 3 3
        QUERY 1 1 1 3 3 3
        2 4 
        UPDATE 2 2 2 1 
        QUERY 1 1 1 1 1 1
        QUERY 1 1 1 2 2 2
        QUERY 2 2 2 2 2 2';
        
        $inputs[] = '3
        4 6
        UPDATE 2 2 2 4
        QUERY 1 1 1 3 3 3
        UPDATE 1 1 1 1000000000
        QUERY 2 2 2 4 4 4
        QUERY 1 1 1 3 3 3
        QUERY 1 1 1 3 3 3
        4 6
        UPDATE 2 2 2 4
        QUERY 1 1 1 3 3 3
        UPDATE 1 1 1 -1000000000
        QUERY 2 2 2 4 4 4
        QUERY 1 1 1 3 3 3
        QUERY 1 1 1 3 3 3
        2 4 
        UPDATE 2 2 2 1 
        QUERY 1 1 1 1 1 1
        QUERY 1 1 1 2 2 2
        QUERY 2 2 2 2 2 2';
        
        
        foreach($inputs as $input) {
            $cube = new \App\Classes\CubeSummation;
            $this->assertTrue($cube->parseInput($input));
        }
    }
    
    /**
     * Test a set of invalid Cube inputs
     *
     * @return void
     */
    public function testInvalidInput()
    {    
        $inputs[] = '5
        4 6
        UPDATE 2 2 2 4
        QUERY 1 1 1 3 3 3
        UPDATE 1 1 1 1
        QUERY 2 2 2 4 4 4
        QUERY 1 1 1 3 3 3
        QUERY 1 1 1 3 3 3
        2 4 
        UPDATE 2 2 2 1 
        QUERY 1 1 1 1 1 1
        QUERY 1 1 1 2 2 2
        QUERY 2 2 2 2 2 2';
        
        $inputs[] = '2
        4 6
        UPDATE 2 2 2 4
        QUERY 1 1 1 3 3 3
        UPDATE 1 1 1 1000000000
        QUERY 2 2 2 4 4 4
        QUERY 1 1 1 3 3 3
        QUERY 1 1 1 3 3 3
        4 6
        UPDATE 2 2 2 4
        QUERY 1 1 1 3 3 3
        UPDATE 1 1 1 -1000000000
        QUERY 2 2 2 4 4 4
        QUERY 1 1 1 3 3 3
        QUERY 1 1 1 3 3 3
        2 4 
        UPDATE 2 2 2 1 
        QUERY 1 1 1 1 1 1
        QUERY 1 1 1 2 2 2
        QUERY 2 2 2 2 2 2';
        
        $inputs[] = '2
        4 6
        UPDATE 2 2 2 4
        QUERY 1 1 1 3 3 3
        UPDATE 1 1 1 1000000000
        QUERY 2 2 2 4 4 4
        QUERY 1 1 1 3 3 3
        4 6
        UPDATE 2 2 2 4
        QUERY 1 1 1 3 3 3
        UPDATE 1 1 1 -1000000000
        QUERY 2 2 2 4 4 4
        QUERY 1 1 1 3 3 3
        QUERY 1 1 1 3 3 3';
        
        
        foreach($inputs as $input) {
            $cube = new \App\Classes\CubeSummation;
            $this->assertFalse($cube->parseInput($input));
        }
    }
    
    /**
     * Test a couple of inputs
     *
     * @return void
     */
    public function testCubeSummation() {
        
        $input = '2
            4 5
            UPDATE 2 2 2 4
            QUERY 1 1 1 3 3 3
            UPDATE 1 1 1 23
            QUERY 2 2 2 4 4 4
            QUERY 1 1 1 3 3 3
            2 4
            UPDATE 2 2 2 1
            QUERY 1 1 1 1 1 1
            QUERY 1 1 1 2 2 2
            QUERY 2 2 2 2 2 2';
        
        $cube = new \App\Classes\CubeSummation;
        $cube->parseInput($input);
        $this->assertEquals($cube->execute(), [4, 4, 27, 0, 1, 1]);
        
        $input = '1
            3 9
            UPDATE 1 1 1 4
            UPDATE 2 2 2 8
            UPDATE 3 3 3 12
            QUERY 1 1 1 3 3 3
            QUERY 2 2 2 2 2 2
            QUERY 2 2 2 3 3 3
            QUERY 1 1 1 2 2 2
            QUERY 1 1 3 1 3 3
            QUERY 3 1 1 3 3 1';
        
        $cube = new \App\Classes\CubeSummation;
        $cube->parseInput($input);
        $this->assertEquals($cube->execute(), [24, 8, 20, 12, 0, 0]);
        
        $input = '1
            3 9
            UPDATE 1 1 1 4
            QUERY 1 1 1 3 3 3
            UPDATE 2 2 2 8
            QUERY 2 2 2 2 2 2
            QUERY 2 2 2 3 3 3
            UPDATE 3 3 3 12
            QUERY 1 1 1 2 2 2
            QUERY 1 1 3 1 3 3
            QUERY 3 1 1 3 3 1';
        
        $cube = new \App\Classes\CubeSummation;
        $cube->parseInput($input);
        $this->assertEquals($cube->execute(), [4, 8, 8, 12, 0, 0]);
    }
}
