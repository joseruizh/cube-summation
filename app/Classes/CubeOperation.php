<?php

namespace App\Classes;

use App\Classes\CubeValidator;

/**
 * Class to handle each operation in a cube summation test case
 *
 * @author jorh
 */
class CubeOperation {
    
    /**
     * @var integer N defines the N * N * N matrix.
     */
    public $N;
    
    /**
     * @var string Type of operation QUERY or UPDATE
     */
    public $type;
    
    /**
     * @var array Contains the parameters of an operation
     */
    public $parameters = [];
    
    public function __construct($N) {
        $this->N = $N;
    }
    
    
    /**
     * parse an opertation from the input
     * @param string &$lines String with the rest of lines in the input
     * @return bool If the format is valid return true else return false 
     */
    public function parseInput(&$lines) {
        
        $operation = trim(array_shift($lines));
        $this->parameters = preg_split('/\s+/', $operation);
        $this->type = trim(array_shift($this->parameters));

        //Validate type of operation
        if(strcmp($this->type, 'UPDATE') != 0 and strcmp($this->type, 'QUERY') != 0) { return false; }

        //Validate all parameters are integers
        if(array_filter($this->parameters, ['App\Classes\CubeValidator','is_int']) != $this->parameters) { return false; }

        if($this->type == 'UPDATE') {
            if(!$this->parseUpdate()) { return false; }
        }

        if($this->type == 'QUERY') {
            if(!$this->parseQuery()) { return false; }
        }
                
        return true;
    }
    
    /**
     * parse an opertation of type UPDATE from the input
     * @param string &$lines String with the rest of lines in the input
     * @return bool If the format is valid return true else return false 
     */
    public function parseUpdate() {
        
        //Validate num of parameters
        if(count($this->parameters) != 4) { return false; }

        //Validate 1 <= x,y,z,... <= N
        for($i = 0; $i < 3; $i++) {
            if(!CubeValidator::in_range($this->parameters[$i], 1, $this->N)) {
                return false;
            }
        }

        //Validate -10^9 <= W <= 10^9
        if(!CubeValidator::in_range($this->parameters[3], -1E+9, 1E+9)) { 
            return false; 
            
        }
        
        return true;
    }
    
    /**
     * parse an opertation of type QUERY from the input
     * @param string &$lines String with the rest of lines in the input
     * @return bool If the format is valid return true else return false 
     */    
    public function parseQuery() {

        //Validate num of parameters
        if(count($this->parameters) != 6) {
            return false;
        }

        //Validate 1 <= d1 <= d2 <= N 
        for($i = 0; $i < 3; $i++) {
            if(!(CubeValidator::in_range($this->parameters[$i], 1, $this->parameters[$i+3]) and 
                CubeValidator::in_range($this->parameters[$i+3], 1, $this->N))
            ) {  
                return false;
            }
        }
        
        return true;
    }
    
    
    /**
     * execute the operation on te cube received as argument
     * @param array &cube Contains a 3D matrix
     * @return bool If there are no errors returns true 
     */ 
    public function execute(&$cube) {
        
        if(strcmp($this->type, 'QUERY') == 0) {
            return $this->executeQuery($cube);                
        }
        if(strcmp($this->type, 'UPDATE') == 0) {
            return $this->executeUpdate($cube);                
        }
        
        return false;
    }
    
    /**
     * execute QUERY type operation on the cube received as argument
     * @param array &cube Contains a 3D matrix
     * @return bool If there are no errors returns true 
     */ 
    public function executeQuery(&$cube) {
        //validate QUERY type
        if(strcmp($this->type, 'QUERY') != 0) { return false; }

        //Validate num of parameters
        if(count($this->parameters) != 6) { return false; }
        
        $x1 = $this->parameters[0] -1;
        $y1 = $this->parameters[1] -1;
        $z1 = $this->parameters[2] -1;
        $x2 = $this->parameters[3] -1;
        $y2 = $this->parameters[4] -1;
        $z2 = $this->parameters[5] -1;
        
        $sum = 0;
        for ($x = $x1; $x <= $x2; $x++) {
            for ($y = $y1; $y <= $y2; $y++) {
                for ($z = $z1; $z <= $z2; $z++) {
                    $sum += $cube[$x][$y][$z];
                }
            }
        }
        
        return $sum;
    }
    
    /**
     * execute UPDATE type operation on te cube received as argument
     * @param array &cube Contains a 3D matrix
     * @return bool If there are no errors returns true 
     */ 
    public function executeUpdate(&$cube) {
        
        //validate UPDATE type
        if(strcmp($this->type, 'UPDATE') != 0) { return false; }

        //Validate num of parameters
        if(count($this->parameters) != 4) { return false; }
        
        $x = $this->parameters[0]-1;
        $y = $this->parameters[1]-1;
        $z = $this->parameters[2]-1;
        $W = $this->parameters[3];
        
        $cube[$x][$y][$z] = $W;       
        
        return true;        
    }    
}
