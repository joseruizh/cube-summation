<?php

namespace App\Classes;

use App\Classes\CubeOperation;
use App\Classes\CubeValidator;

/**
 * Class for modeling each test case in cube summation
 *
 * @author jorh
 */
class CubeTestCase {
    
    /**
     * @var integer N defines the N * N * N matrix. 
     */
    public $N;
    
    /**
     * @var integer M defines the number of operations. 
     */
    public $M;
    
    /**
     * @var array Matrix (N*N*N) contains the values of the cube
     */
    public $cube = [];
    
    /**
     * @var array Contains the operations of the test case.
     */
    public $operations = [];
    
    public function __construct() {
        
    }       
    
    /**
     * parse a test case from the input
     * @param string &$lines String with the rest of lines in the input
     * @return bool If the format is valid return true else return false 
     */
    public function parseInput(&$lines) {
        
        $case_params = preg_split('/\s+/', trim(array_shift($lines)));
            
        //validate exactly 2 values and integers
        if(count($case_params) != 2  or 
            array_filter($case_params, ['App\Classes\CubeValidator','is_int']) != $case_params) {
            
            return false;
        }
        
        $this->N = $case_params[0];
        $this->M = $case_params[1];

        //Validate 1 <= N <= 100
        if(!CubeValidator::in_range($this->N, 1, 100)) { return false; }

        //Validate 1 <= M <= 100
        if(!CubeValidator::in_range($this->M, 1, 100)) { return false; }

        for($index_oper = 0; $index_oper < $this->M; $index_oper++) {  
            
            $cube_operation = new CubeOperation($this->N);
            if(!$cube_operation->parseInput($lines)) { return false; }
            
            $this->operations[] = $cube_operation;
        }
        
        //initialize the cube
        $this->cube = array_fill(0, $this->N, array_fill(0, $this->N, array_fill(0, $this->N, 0)));
        return true;
    }
    
    /**
     * execute an entire test case on the cube
     * @return array contains the ouput for the test case
     */ 
    public function execute() {
        
        $output = [];
        
        foreach($this->operations as $operation) {
            $result = $operation->execute($this->cube);
            if($result === false) { 
                return false;    
            }
            
            if(is_int($result)) $output[] = $result;
        }
        
        return $output;
    }    
}
