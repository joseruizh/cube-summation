<?php

namespace App\Classes;

/**
 * Class with some validators for cube summation
 *
 * @author jorh
 */
class CubeValidator {
    
    public static function is_int($val) {        
        return filter_var($val, FILTER_VALIDATE_INT);
    }
    
    public static function in_range($number, $min, $max, $inclusive = true) {
        
        return $inclusive
            ? ($number >= $min && $number <= $max)
            : ($number > $min && $number < $max) ;
    }
}
