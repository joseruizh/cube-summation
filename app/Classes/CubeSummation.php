<?php

namespace App\Classes;

use App\Classes\CubeTestCase;
use App\Classes\CubeValidator;

/**
 * Cube Summation Class
 *
 * @author jorh
 */
class CubeSummation {
    
    /**
     * @var integer Number of test cases
     */
    public $T;
    
    /**
     * @var array Array of CubeTestCase objects
     */
    public $test_cases = [];
    
    
    public function __construct() {
        /* empty */
    }
    
    /**
     * parse and validate a text inupt
     * @param string $text Input Text with a set of test cases for cube summation
     * @return bool If the format is valid return true else retur false 
     */
    public function parseInput($text) {
       
        $lines = explode("\n", trim($text));
                
        //At least 3 lines
        if(count($lines) < 3) {
            return false;
        }
        
        if(!$this->parseFirstLine($lines)) { return false; }
        
        for($index_case = 0; $index_case < $this->T; $index_case++) {
            
            $test_case = new CubeTestCase();
            
            if(!$test_case->parseInput($lines)) { return false; }
            
            $this->test_cases[] = $test_case;
        }
        
        //Validate that there are no more cases
        if(count($lines)) {
            return false;
        }
        
        //if everything is fine
        return true;
    }
    
    /**
     * parse the first line from the input, it should have an integer
     * @param string &$lines String with the rest of lines in the input
     * @return bool If the format is valid return true else return false 
     */
    public function parseFirstLine(&$lines) {
        
        // validate 1 <= T <= 50
        $this->T = trim(array_shift($lines));
        if(!filter_var($this->T, FILTER_VALIDATE_INT) or 
           !CubeValidator::in_range($this->T, 1, 50)) {
            return false;
        }
        
        return true;
    }
    
    
    /**
     * execute a cube summation input and generate an output
     * @return array contains the ouput for cube summation
     */
    public function execute() {
        
        $output = [];
        
        foreach($this->test_cases as $t) {
            
            $o = $t->execute();
            
            if($o === false) { return false; }
            
            $output = array_merge($output, $o);
        }
        
        return $output;
    }
}
