<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * @param Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $vars = [];   
        $vars['input'] = '';
        $vars['output'] = $request->get('output');
        
        $vars['inputs'] = Auth::user()->inputs()
            ->orderby('created_at', 'desc')->get();
        
        return response()->view('home', $vars);
    }
    
    /**
     * Execute the input and return an output result
     * @param Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function execute(Request $request)
    {
        
        if (!$request->ajax())
        {
            return response('Unauthorized.', 401);
        }
        
        $input = $request->get('input');
        
        $cube_summation = new \App\Classes\CubeSummation();
        
        if($cube_summation->parseInput($input)) {            
            $result = $cube_summation->execute();
            return response()->json(['result' => implode("\n", $result)]);
        }
        
        return response()->json(['error' => 'The input supplied is not valid.']);        
        
    }
        
    /**
     * Save a valid input in the user's input list
     * @param Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        
        if (!$request->ajax())
        {
            return response('Unauthorized.', 401);
        }
        
        $input = $request->get('input');
        
        $cube_summation = new \App\Classes\CubeSummation();
        
        if($cube_summation->parseInput($input)) {
            
            $input = new Input();
            $input->user_id = Auth::user()->id;
            $input->title = $request->get('title');
            $input->input = trim($request->get('input'));
            $input->save();
            return response()->json([
                'result' => 'The input was successfully saved.',
                'id' => $input->id,
                'input' => nl2br($input->input),
                'input_raw' => $input->input
            ]);
        }
        
        return response()->json(['error' => 'The input supplied is not valid.']);        
        
    }    
        
    /**
     * Delete an input from the user's input list
     * @param Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        
        if (!$request->ajax())
        {
            return response('Unauthorized.', 401);
        }
                
        
        if(!$input = Auth::user()->inputs()->find($request->get('id'))) {
            return response()->json(['error' => 'The input does not exist.']);
        }
        $input->delete();
        return response()->json(['result' => 'The input was successfully deleted.']);        
        
    }   
        
    /**
     * Load a valid input from a text file
     * @param Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */    
    public function upload_input(Request $request) {
        
        if (!$request->ajax())
        {
            return response('Unauthorized.', 401);
        }
        //Validate the file
        $this->validate($request, [
            'filedata' => 'required|mimetypes:text/plain|max:1024',
        ]);
        
        //Get the content of the file
        $file = $request->filedata;
        $tmp_path = $file->getRealPath();
        $content = \Illuminate\Support\Facades\File::get($tmp_path);
        
        $cube_summation = new \App\Classes\CubeSummation();        
        if(!$cube_summation->parseInput($content)) {
            return response()->json(['error' => 'The input supplied is not valid.']);     
        }
        return response()->json([
            'result' => 'The file was successfully loaded.',
            'input' => trim($content)
        ]);
        
    }
}
