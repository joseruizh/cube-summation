# Cube Summation
this is a laravel application for resolving the cube summation problem described in [hackerrank.com](https://www.hackerrank.com/challenges/cube-summation)

### Installation
You need mysql 5, php 5.6 and composer installed on your machine
First you need to clone the project
```sh
$ git clone https://bitbucket.org/joseruizh/cube-summation.git
```
Second, you need to create a mysql database and add your configuration in the file `cube-summation/.env`

Then, you should to run the following commands
```sh
$ composer install
$ php artisan migrate
$ php artisan serve
```
The first command will install all the dependencies of the project.
The second will generate the tables in the database
And the last command run a development server and you will be able to access the aplication from `http://localhost:8000`


### Demo
You can see the live demo in the following link [Cube Summation](http://cubesummation.magohost.com.ve)
