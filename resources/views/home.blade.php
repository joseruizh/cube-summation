@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Cube Summation Processor</div>

                <div class="panel-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Input</label>
                        <textarea class="form-control input-text" name="input" id="input" rows="15">{{ $input }}</textarea>
                    </div>
                    <button type="button" id="execute" class="btn btn-primary">Execute</button>
                    <button type="button" class="btn btn-default"  data-toggle="modal" data-target="#title-modal">Save</button>
                    <button type="button" class="btn btn-link" id="reset">Reset</button>
                    <div id="output-container">
                        <label>Output</label>
                        <textarea class="form-control input-text" name="output" id="output" rows="15" disabled>{{ $output }}</textarea>
                    </div>                
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Load input from a file</div>

                <div class="panel-body">
                    <form id="form-file" method="post" action="{{ url('/upload-input') }}" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" id="input-file" name="filedata">
                        </div>                    
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>            
            <div id="panel-input-list" class="panel panel-default">
                <div class="panel-heading">Load input from the list</div>
                    
                <div class="panel-body" id="body-list">                    
                    <div class="alert alert-danger" id="no-inputs" @if(count($inputs)) style="display:none" @endif>There are no saved inputs.</div>
                    
                    @foreach($inputs as $input)
                    <div class="panel panel-primary panel-input-item" id="input-panel-{{ $input->id }}">
                        <div class="panel-heading"><a href="javascript: void(0)" class="input-title" data-id="{{ $input->id }}">Load {{ $input->title }}</a>
                            <button type="button" class="close"  data-toggle="modal" data-target="#delete-modal" data-id="{{ $input->id }}"> <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        </div>
                        <div class="panel-body panel-body-input-item input-text">
                            <div><label>Input:</label></div>
                            <span class="span-input-text" id="input-text-{{ $input->id }}" data-input="{{ $input->input }}">{!! nl2br($input->input) !!}</span>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
            </div>            
        </div>
    </div>
</div>


<div class="modal fade" id="title-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body"> 
                <form id="form-save">
                    <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" id="title" type="text" required="true" maxlength="25">
                    </div>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body"> 
                <div>Are you sure you want to delete this item?</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary" id="delete-confirm">Delete</button>
                <input type="hidden" id="delete-input-value" value="">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="error-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Error</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="info-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Info</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    @parent
    <script>
    $(document).ready(function() {
        //Call execute funtion when click in execute button
        $("#execute").click(function() {
            execute($('#input').val());
        });

        //Clear input & output fields when click in reset button
        $("#reset").click(function() {
            $("#input").val('');
            $("#output").val('');
        });

        //Call save function with the title value
        $("#form-save").submit(function(event) {
            event.preventDefault();
            save($('#title').val(), $('#input').val());
            $('#title').val('');
        });

        //Call function for loading an input from the list
        $(document).on('click', '.input-title', function() {
            load_input_text($(this).attr('data-id'));               
        });

        //Fill in hidden field with the clicked input id
        $(document).on('click', 'button.close', function() {
            $("#delete-input-value").val($(this).attr('data-id'));            
        });

        //Call function for deleting an input from the list
        $(document).on('click', '#delete-confirm', function() {
            var id = $("#delete-input-value").val();
            delete_input(id);
        });
        
        var file;
        $('#input-file').change(function(){
           file = event.target.files;
        });
        //Call upload file function
        $("#form-file").submit(function(event) {
            event.stopPropagation();
            event.preventDefault();
            upload_file(file);
        });
    });

    //Execute an input
    function execute(input) {
        $.ajax({
            method: "POST",
            url: "{{url('/execute')}}",
            cache: false,
            data: { input: input, _token: '{{ csrf_token() }}'}
        })
        .done(function( msg ) {
            if(msg.result) {
                $('#output').val(msg.result);
            }
            else {
                $('#output').val('');
                $('#error-modal .modal-body').text(msg.error);
                $('#error-modal').modal('show');
            }
        });
    }        

    //Save an input in the list
    function save(title, input) {
        $.ajax({
            method: "POST",
            url: "{{url('/save')}}",
            cache: false,
            data: { title: title, input: input, _token: '{{ csrf_token() }}'}
        })
        .done(function( msg ) {
            $('#title-modal').modal('hide');
            if(msg.result) {
                add_input_to_list(msg.id, title, msg.input, msg.input_raw);
                $('#info-modal .modal-body').text(msg.result);
                $('#info-modal').modal('show');
            }
            else {
                $('#error-modal .modal-body').text(msg.error);
                $('#error-modal').modal('show');
            }
        });
    }

    //Add the input saved to the list
    function add_input_to_list(id, title, input, input_raw) {
        var target = '<div class="panel panel-primary  panel-input-item" id="input-panel-'+id+'">';
        target += '<div class="panel-heading"><a href="javascript: void(0)" class="input-title" data-id="'+id+'">Load '+title+'</a>';
        target += '<button type="button" class="close"  data-toggle="modal" data-target="#delete-modal" data-id="'+id+'">';
        target += '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>';
        target += '<div class="panel-body panel-body-input-item input-text">';
        target += '<div><label>Input:</label></div>';
        target += '<span class="span-input-text" id="input-text-'+id+'" data-input="'+input_raw+'">'+input+'</span></div></div>';

        $("#panel-input-list #body-list").prepend(target); 
        show_hide_no_inputs();
    }

    //Load an input from the list to the input field
    function load_input_text(id) {
        var input = $("#input-text-"+id).attr('data-input');
        $('#input').val(input);
    }

    //Delete an input from the list
    function delete_input(id) {
        $.ajax({
            method: "POST",
            url: "{{url('/delete')}}",
            cache: false,
            data: { id: id, _token: '{{ csrf_token() }}'}
        })
        .done(function( msg ) {
            $('#delete-modal').modal('hide');
            if(msg.result) {
                $("#input-panel-"+id).remove();
                show_hide_no_inputs();
                $('#info-modal .modal-body').text(msg.result);
                $('#info-modal').modal('show');
            }
            else {
                $('#error-modal .modal-body').text(msg.error);
                $('#error-modal').modal('show');
            }
        });
    }

    //Show or hide "no items" message in the list
    function show_hide_no_inputs() {
        if($('.panel-input-item').length > 0) {
            $("#no-inputs").hide();
        } else {
            $("#no-inputs").show();
        }
    }
    
    //Uplad a text file
    function upload_file(file) {
        var data = new FormData();
        data.append('filedata', file[0]);
        data.append( '_token', '{{ csrf_token() }}');
        $.ajax({
            url: $('#form-file').attr('action'),
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            error: function(jqXHR, textStatus, errorThrown) {
                $('#output').val('');
                $('#error-modal .modal-body').text(JSON.parse(jqXHR.responseText).filedata);
                $('#error-modal').modal('show');
            }
        })            
        .done(function( msg ) {
            if(msg.result) {
                $('#output').val('');
                $("#input").val(msg.input);
                $('#info-modal .modal-body').text(msg.result);
                $('#info-modal').modal('show');
            }
            else {
                $('#error-modal .modal-body').text(msg.error);
                $('#error-modal').modal('show');
            }
        });
    }
    
    </script>
@endsection
